import { render } from '@testing-library/react'
import ProductListItem from './product-list-item'
import '@testing-library/jest-dom'

describe('Product List Item', () => {
   it('renders title, price with £ & image on listing page', async () => {
    const data = {
        title: 'Bosch Serie 2 SGS2ITW41G Freestanding Dishwasher, White',
        price : {
            now: '369.00'
        },
        image: '//johnlewis.scene7.com/is/image/JohnLewis/240251283?'
    }
    const {getByText, getByAltText} = render(<ProductListItem price={data.price} image={data.image} title={data.title} />)

    const title = getByText(data.title)
    const price = getByText(`£ ${data.price.now}`)
    const image = getByAltText(data.image)
    expect(title).toBeVisible()
    expect(price).toBeVisible()
    expect(image).toBeInTheDocument()
    });
})