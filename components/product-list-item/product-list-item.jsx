import styles from "./product-list-item.module.scss";
import Image from 'next/image'

const ProductListItem = ({ image, price, title }) => {
  return (
    <div className={styles.content}>
      <div className={styles.img}>
        <Image src={`https:${image}`} alt={image} width="100%" height="auto" layout="responsive" objectFit="contain"/>
      </div>
      <div className={styles.blurbPadded}>{title}</div>
      <div className={styles.price}>£ {price.now}</div>
    </div>
  );
};

export default ProductListItem;
