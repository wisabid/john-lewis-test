import {useState, useEffect} from "react";
import styles from "./more.module.scss"

export default function more({descText}) {
    const [readmore, setReadmore] = useState(false);
    const [desc, setDesc] = useState('');

    useEffect(() => {
        let description = descText;
        if (description.length > 500) {
            setReadmore(prev => !prev);
            description = `${description.substr(0,500)}...`;
        }
        setDesc(description);
    }, [descText])
    
    return (
        <>
            <div dangerouslySetInnerHTML={{ __html: desc}} className={styles.desc}></div>
            {readmore && <h2 className={styles.readmore} onClick={() => {setDesc(descText); setReadmore(prev => !prev)}}>Read more <span>{`>`}</span></h2>}
        </>
    )
}