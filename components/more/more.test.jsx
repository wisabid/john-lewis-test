import { fireEvent, render } from '@testing-library/react'
import More from './more'
import '@testing-library/jest-dom'
import { act } from 'react-dom/test-utils';

describe('More', () => {
    it('should render trimmed description with a read more link & on click the link should disappear', async () => {
        const productInformationRaw = "<P>This slimline integrated dishwasher tackles dirty dishes so you don't have to. Ideal for compact kitchen spaces, a 60-minute quick wash option provides clean dishes in a cinch. SpeedPerfect shortens your favourite washing cycle (handy for any special or delicate washes), and ExtraDry means you can throw away that raggedy-old tea towel too. Those difficult-to-dry bits like Tupperware are taken care of with just the push of a button. Bonus.<\/p>\n\n<p><b>EcoSilence Drive™ keeps noise to a minimum<\/b><br>\nSleep soundly even with the dishwasher running as EcoSilence Drive™ keeps noise to a minimum. The brushless, energy-saving motor not only ensures you won't be disturbed at the night but also helps give the machine a longer working life; all without sacrificing cleaning power.<\/p>\n\n<p><b>DosageAssist provides optimum results<\/b><br>\nFrom greasy baking trays to heavily soiled pans, DosageAssist ensures all your kitchen crockery comes out crystal clean. Catching the tablet in a special tray on the top basket, it gets completely dissolved, giving you great results every time.<\/p>\n\n<p><b>InfoLight tells you when the cycle has finished<\/b><br>\nYou'll always know when your dishes are done, thanks to the handy InfoLight feature. It projects a red light onto the floor when the dishwasher is running, so you won't accidentally open the door halfway through the cycle.<\/p>\n\n<p><b>Efficient water useage<\/b><br>\nBosch's ActiveWater technology keeps your water usage low and your monthly bills even lower. By combining targeted water circulation with faster heat-up times and improved pressure in the spray arms, your dishwasher gets the maximum use out of each and every drop.<\/p>\n\n<p><b>Dry dishes every time<\/b><br>\nWith the convenient ExtraDry function, you can say goodbye to finishing the job with a tea towel. Even difficult-to-dry loads will come out dry and ready to use.<\/p>\n\n<p><b>SpeedPerfect reduces wash cycle time<\/b><br>\nReducing washing times by up to 65%, this time-saving feature comes in super handy if you need to shorten the run time of the intensive or eco programmes.<\/p>";
        const productInformation = productInformationRaw.replace(/(<([^>]+)>)/gi, "");
        const {getByText} = render(<More descText={productInformation} />)
        const readMoreElem = getByText('Read more');
        expect(readMoreElem).toBeVisible();
        await act(async() => {
            fireEvent.click(readMoreElem);
        })
        expect(readMoreElem).not.toBeVisible();
    })

    
})