import { render } from '@testing-library/react'
import Header from './header'
import '@testing-library/jest-dom'

describe('More', () => {
    it('should render title without a back button', async () => {
        const title = "Dishwashers";
        const {queryByText, getByText} = render(<Header title={title} />)
        const titleElem = getByText(title);
        const back = queryByText('<');
        expect(titleElem).toBeVisible();
        expect(back).toBeNull()
    })

    it('should render title and back button', async () => {
        const title = "Dishwashers";
        const {queryByText, getByText} = render(<Header title={title} back={true} />)
        const titleElem = getByText(title);
        const back = queryByText('<');
        expect(titleElem).toBeVisible();
        expect(back).toBeVisible();
    })

    
})