import { useRouter } from 'next/router'
import styles from "./header.module.scss"

export default function header({title, back=false}) {
    const router = useRouter()
    return (
        <>
            <header className={styles.header} style={{justifyContent: 'center'}}>
                {back && <button className={styles.backBtn} onClick={() => router.back()}>{`<`}</button>}
                <h1 className={styles.headerTitle}>{title}</h1>
            </header>
        </>
    )
}