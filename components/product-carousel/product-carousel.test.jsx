import { fireEvent, render } from '@testing-library/react'
import ProductCarousel from './product-carousel'
import '@testing-library/jest-dom'
import { act } from 'react-dom/test-utils';

describe('Product Carousel', () => {
    it('renders first images in the list as default image', async () => {
        const images = ["\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt1?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt2?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt3?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt4?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt5?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt6?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt7?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt10?"]
        const {getByAltText} = render(<ProductCarousel images={images} />)

        const defaultimage = getByAltText(images[0])
        expect(defaultimage).toBeInTheDocument();
    });

    it('should render scrolling dots based on the number of images passed & clicking should render corresponding image', async () => {
        const images = ["\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt1?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt2?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt3?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt4?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt5?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt6?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt7?", "\/\/johnlewis.scene7.com\/is\/image\/JohnLewis\/240251287alt10?"]
        const {queryAllByText, getByAltText} = render(<ProductCarousel images={images} />)
        const scrollDots = queryAllByText('.');
        expect(scrollDots).toHaveLength(images.length)
        await act(async() => {
            fireEvent.click(scrollDots[3]);
        })
        expect(getByAltText(images[3])).toBeInTheDocument();
    })

    
})