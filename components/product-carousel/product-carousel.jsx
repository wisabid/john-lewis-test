import { nanoid } from "nanoid";
import {useState} from "react";
import styles from "./product-carousel.module.scss";
import Image from 'next/image'


const ProductCarousel = ({ images }) => {
  const [selectedImage, setSelectedImage] = useState(images.length > 0 && images[0])

  const dotElems = images.map(item => 
    <li key={nanoid()} onClick={() => setSelectedImage(item)} className={selectedImage === item?styles.selected:styles.lidot}>{'.'}</li>
  )
  return (
    <div className={styles.productCarousel}>
      <div style={{width: '100%', height: '100%', position: 'relative'}}>
        <Image src={`https:${selectedImage}`} alt={selectedImage} className={styles.img} 
          width="100%" height="100%" layout="responsive" objectFit="contain"/>
      </div>
      <div style={{fontSize: '100px', position: 'absolute', bottom: '-40px'}}>
        <ul style={{listStyle:'none', display: 'flex', margin: '0', padding: '0'}}>
          {dotElems}
        </ul>
      </div>
    </div>
  );
};

export default ProductCarousel;
