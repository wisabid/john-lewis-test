module.exports = {
    images: {
      domains: ['assets.vercel.com', 'johnlewis.scene7.com'],
      formats: ['image/avif', 'image/webp'],
    },
  }