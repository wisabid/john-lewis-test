# UI Dev Technical Test - My Dishwasher App

### Product Grid

When the website is launched, the customer is presented with a grid of dishwashers that are currently available for customers to buy. 

In this exercise we are only displaying the first 20 results returned by the API. A url param `&pageSize=20` has been used on api url to achieve this.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

## Features

- The website is fully responsive, working across device sizes. 
- The following third party libraries are used :
    1. axios - This is for fetch on server side as node-fetch had some build issues.
    2. nanoid - Unique id generation
    3. babel-jest, @testing-library/jest-dom, @testing-library/react - To support test environment.
- Unit tests are in place and there is still a lot to achieve.

---

## How to run?

- `Clone this repo` into your local directory using `git clone https://gitlab.com/wisabid/john-lewis-test.git` (you will need a GitLab account).
- Install the dependencies using `yarn`
- Run the development server with `yarn dev`
- Open [http://localhost:3001](http://localhost:3001) with your browser.
