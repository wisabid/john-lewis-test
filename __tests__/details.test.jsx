import { render, fireEvent, screen, wait, waitFor } from '@testing-library/react'
import Details from '../pages/product-detail/[id]'
import product from "../mockData/productData.json"
import '@testing-library/jest-dom'

describe('Home', () => {
  it('renders a heading & items', async () => {
    render(<Details data={product} />)

    const heading = screen.getByRole('heading', {
      name: /Bosch Serie 2 SRV2HKX39G Fully Integrated Slimline Dishwasher/i,
    })
    expect(heading).toBeInTheDocument()
  })
})