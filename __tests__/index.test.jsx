import { render, fireEvent, screen, wait, waitFor } from '@testing-library/react'
import Home from '../pages/index'
import products from "../mockData/allProducts.json"
import '@testing-library/jest-dom'

describe('Home', () => {
  it('renders a heading & items (displaying title & price)', async () => {
    const {queryByText} = render(<Home data={products} />)

    const heading = screen.getByRole('heading', {
      name: /Dishwashers/i,
    })
    expect(heading).toBeInTheDocument()
    await waitFor(() => {
      const linkElem = screen.getByRole('link', {
        name: /Bosch Serie 2 SGS2ITW41G Freestanding Dishwasher, White/i,
      });
      expect(linkElem).toBeInTheDocument()
      expect(queryByText('£ 369.00')).toBeVisible();
    });
  })
})