import axios from 'axios';
import Head from "next/head";
import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "./product-detail.module.scss"
import Header from "../../components/header/header"
import More from "../../components/more/more"
import {nanoid} from "nanoid";

export async function getServerSideProps(context) {
  const id = context.params.id;
  const response = await axios.get(
    "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
  );
  const data = response.data;
  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  const detailedElements = data.details.features[0].attributes.map((item) => (
    <li key={nanoid()}>
      <div><div dangerouslySetInnerHTML={{ __html: item.name }} /></div>
    </li>
  ))
  return (
    <>
      <Head>
          <title>JL &amp; Partners | {data.title}</title>
          <meta name="keywords" content={`shopping, ${data.title}`} />
        </Head>
      <Header title={data.title} back={true}/>
      
      <div className={styles.dividedSec}>
        <ProductCarousel images={data.media.images.urls} />
        <div className={styles.priceSec}>
          <h1 style={{textAlign:'left'}}>£{data.price.now}</h1>
          {data.displaySpecialOffer && <p style={{color:'red'}}>{data.displaySpecialOffer}</p>}
          <p style={{fontWeight: 'bold'}}>{data.additionalServices.includedServices}</p>
        </div>
      </div>

      <div className={styles.dividedSec}>
        <div className={styles.details}>
          <h3>Product information</h3>
          <p>Product code: {data.code}</p>
          <More descText={data.details.productInformation} />
          <h3>Product specification</h3>
          <ul className={styles.specUl}>
            {detailedElements}
          </ul>
        </div>
      </div>
        
    </>
  );
};

export default ProductDetail;
