import Head from "next/head";
import Link from "next/link";
import axios from 'axios';
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import Header from "../components/header/header";

export async function getServerSideProps() {
  const response = await axios.get(
    "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI&pageSize=20"
  );
  const data = response.data;
  return {
    props: {
      data: data,
    },
  };
}

const Home = ({ data }) => {
  let items = data.products;
  const listElements = items.map((item) => (
    <Link
      key={item.productId}
      href={{
        pathname: "/product-detail/[id]",
        query: { id: item.productId },
      }}
    >
      <a className={styles.link}>
        <ProductListItem price={item.price} image={item.image} title={item.title} />
      </a>
    </Link>
  ))
  return (
    <>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <Header title="Dishwashers" />
      <section className={styles.content}>
        {listElements}
      </section>
    </>
  );
};

export default Home;
